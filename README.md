


## <i> <b> Primeira Atividade Prática Guilherme Iannini

## <b> 3)

![calculadora_1](/uploads/ce2e4514dfbf0bccc6875f7d6a2772eb/calculadora_1.png)

 o código fornecido para a calculadora js faz o seguinte:

 **const args = process.argv.slice(2);**  irá pegar os 2 argumentos que fornecemos para serem processados.

  **"console.log"** irá exibir a resposta.
  
  o parseInt irá transformar um caracter em um número inteiro, isso porque quando passamos um número para o terminal inicialmente ele o observa como um string, por isso é necessário essa conversão.

 <i>  portanto esse trecho de código lê dois números e faz a soma dos mesmos.





 <b> para executa-lo no meu terminal é preciso utilizar o comando como na imagem:(obs: estou utilizando o visual studio code)

 ![questão_3](/uploads/21a2ab51ac76718a0f436071c6df954b/questão_3.png)



 ## <b> 4)

  **R:** É possível utilizar o comando git add calculadora.js ( que foi o que escolhi) ou o comanto git add –all, que adicionaria todos os arquivos no estado “untracked”
para fazer o push de calculadora.js utilizei: "git push main master"



## <b> 5)
**R:** a primeira alteração em calculadora.js 


![calculadora_2](/uploads/08fbc5f5bf490eab5e5297ab27fa832f/calculadora_2.png)


quando a calculadora.jd foi alterada, seu status mudou de unmodified para modified, logo precisei alterar novamente seu status para staged.

Por fim  fiz o push da modificação para o meu repositório Atividade Pratica 

## <b> 6)

**R:** alteração feita por "João"

![calculadora_3](/uploads/35dbd39218291fb97645da2aa060185a/calculadora_3.png)


Correção de código de joão:
![6](/uploads/2f2331275955201abf0e6f51aa9fc431/6.png)

depois de consertar o código, realizei o commit do mesmo.

## <b> 7)
 <b> criando  branch " develop"

 ![7.1](/uploads/cf19bbd16519face1792d01c1c4a7525/7.1.png)
 

<b> desenvolvendo a feature de divisão nessa branch

![7.2](/uploads/e62c4bc30b71dadb0ad5c0662e233bc0/7.2.png)




## <b> 8)
<b> fazendo o merge

![8](/uploads/0a8f08af29faa9913359c456051b3546/8.png)

## <b> 9) segunda alteração de "João"

![calculadora_4](/uploads/4990a7323cd04996468c49617a51bd37/calculadora_4.png)

Desfazendo a Alteração feita por joão:

![9.1](/uploads/e4e41742ccf8b9b8a8bc3957f17d7204/9.1.png)

como quero desfazer o último git, o hash que irei passar é : b90ceb20112e8fd425bdd598bc850c44a4ecf672

![9.2](/uploads/909e5f893fb926a9a7e472ca7cd47e9c/9.2.png)

agora a correção de joão foi desfeita na minha máquina, fiz um novo commit com os consertos para o gitlab.




## <b> 10)
**R:**
o código fornecido por joão, ainda é uma calculadora, feita de uma forma diferente.

**var x = args[0];**

**var y = args[2];**

**var operator = args[1];**

irão criar as variaveis x,y e operator e armazenar os dados que forem digitados no momento da execução nessas respectivas variáveis.

<b>function evaluate(param1, param2, operator) {

   <b>return eval(param1 + operator + param2);

   <b> }

   essa função irá processar os dados e irá realizar a operação com base no operador que foi fornecido e armazenado na variável "operator"

   Por fim, para executar o código, digitamos: <b> node.[nome do arquivo] [numero] [operador] [numero]





